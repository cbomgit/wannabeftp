#include "serverFunctions.h"
#include "protocol.h"
#include "debug.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdint.h>


int main(int argc, char *argv[]) {

   int serverSocket, clientSocket, bytes_recv;
   socklen_t sin_size;
   struct sockaddr_storage their_addr;
   char s[INET6_ADDRSTRLEN];
   char *msg;
   
   int contentLength;
   uint32_t *packedLength;

   serverSocket = buildConnection();

   //now loop and accept connections
   while(1) {
      
      sin_size = sizeof their_addr;
      clientSocket = accept(serverSocket, (struct sockaddr *)&their_addr, &sin_size);
      if(clientSocket == -1) {
         perror("accept");
         continue;
      }

      #ifdef DEBUG
         inet_ntop(their_addr.ss_family,get_in_addr((struct sockaddr *)&their_addr),s,sizeof s);
         printf("server: got connection from %s\n", s);
      #endif

      catalog(clientSocket);
       
      printf("Closing connection with client.\n");
      close(clientSocket);
      
   }

   #ifdef DEBUG
      printf("Closing server connection.\n");
   #endif

   close(serverSocket);
   close(clientSocket);
    

   return 0;
}
   

