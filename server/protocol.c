#include"protocol.h"
#include"serverFunctions.h"
#include<sys/stat.h>
#include"debug.h"
#include"codes.h"
#include<dirent.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include<fcntl.h>
#include<unistd.h>
#include<arpa/inet.h>

#ifdef DEBUG
   #include<stdio.h>
#endif

int find(char *fileName) {

   DIR *d;
   struct dirent *dir;
   d = opendir(".");

   if(d) {
      while((dir = readdir(d)) != NULL) {
         if(dir->d_type == DT_REG && strcmp(dir->d_name, fileName) == 0) {
            return 1;
         }
      }
   }

   return 0;

}


void ls(char **listing) {

   DIR *d; 
   struct dirent *dir;
   d = opendir(".");
   unsigned int max_length = NAME_MAX, bytes_written = 0;

   #ifdef DEBUG
      printf("Allocating memory to *listing\n");
   #endif
   *listing = (char*) malloc(sizeof(char*) * max_length);

   //reading 
   if(d) {

      while((dir = readdir(d)) != NULL) {
         if(dir->d_type == DT_REG) {
            if(sizeof(char*) + bytes_written + (strlen(dir->d_name) * sizeof(char*)) > max_length) {
               #ifdef DEBUG
                  printf("Increasing memory from %d to %d bytes\n", max_length, (int)strlen(*listing) * 2);
               #endif
               max_length =max_length * 2;
               *listing = (char*) realloc(*listing, sizeof(char*) + (max_length));
               #ifdef DEBUG
                  if(*listing == NULL) { 
                     printf("Listing is null.\n"); 
                  }
               #endif
            }
            strcat(*listing, dir->d_name);
            strcat(*listing, "\n");
         }
      }
   }
   else {
      #ifdef DEBUG
         printf("Directory was empty. Writing a blank line.\n");
      #endif
      strcat(*listing, ". .. \n");
   }

}

int catalog(int socket) {

   #ifdef DEBUG
      printf("Entering catalog() in current directory.\n");
   #endif

   char *listing;
   int strLength, status;
   uint32_t packed;

   ls(&listing);
   strLength = strlen(listing);
   packed = htonl(strLength);

   #ifdef DEBUG
      printf("Sending directory listing to client.\n");
      printf("%d characters long.\n", strLength);
   #endif

   if((status = sendAll(socket, &packed, sizeof(uint32_t))) == -1) {
      perror("catalog: send()");
      return -1;
   }

   if((status = sendAll(socket, listing, strLength)) == -1) {
      perror("catalog: size send()");
      return -1;
   }

   #ifdef DEBUG
      printf("Successfully sent directory contents to client.\n");
      printf("Sent %d bytes of data.\n", status);
   #endif

   return 0;
}


//This function will send a file to the client addressed
//by the passed socket. Replies with a 0 to the client
//if file is not found.
int dcopy(int socket, char *file) {

   int fd, fileSize = 0, chunkSize = 4096;
   int bytesRead = 0, totalRead;
   struct stat st;
   off_t offset;
   uint32_t *packed;
   void *buffer;

   //if the file does not exist, send 0 to the client
   if(!find(file)) {
      *packed = htonl(fileSize);
      sendAll(socket, &packed, sizeof(uint32_t));
   }
   else {
      //get the size of the file and send it to the client
      stat(file, &st);
      fileSize = st.st_size;
      *packed = htonl(st.st_size);
      sendAll(socket, &packed,  sizeof(uint32_t));

      //allocate a buffer for reading from the file
      sendAll(socket, &packed,  sizeof(uint32_t));
      buffer = (void*) malloc(sizeof(void*) * chunkSize);

      //open the file for reading
      fd = open(file, O_RDONLY);
      if(fd == -1) {
         fprintf(stderr, "Could not open file for reading.\n");
         exit(EXIT_FAILURE);
      }

      offset = 0;
      //read the file in chunks of chunkSize, stopping 
      //once we've read every byte. Send to the client after each read
      while(totalRead < fileSize) {
         bytesRead = pread(fd, buffer, chunkSize, offset);
         if(bytesRead == -1) {
            close(socket);
            free(buffer);
            exit(EXIT_FAILURE);
         }
         
         totalRead += bytesRead;
         offset = totalRead;

         if((sendAll(socket, buffer, bytesRead)) == -1) {
            free(buffer);
            close(socket);
            exit(EXIT_FAILURE);
         }
      }

      close(fd);
      free(buffer);
   }

   return 0;
 
}

int ucopy(int socket, char *file) {

   //4 byte int for transmission
   uint32_t packedData;
   int fileExists, fd; //file descriptor
   int bytesSent, bytesRecv, totalRecv;

   //sets the correct user permissions
   mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
   //size fo the file and the maximum amount of bytes we will read
   //from the socket per file write
   int fileSize, chunkSize = 4096;
   
   //buffer to hold the data, allocated to chunk size
   void *buffer;
   off_t offset;

   //does the file exist?
   fileExists = find(file);
   packedData = htonl(fileExists);

   //send the result of fileExists as a reply to the client
   //if the result is 0, the client will send the file over
   if((bytesSent = sendAll(socket, &packedData, sizeof(uint32_t))) == -1) {
         perror("Error sending reply. sendAll()");
         return -1;
   }
   

   if(!fileExists) {
      //prepare to receive the file
      if((bytesRecv = recvAll(socket, &packedData, sizeof(uint32_t))) == -1) {
         perror("Error receiving file size. recvAll()");
         return -1;
      }

      fileSize = ntohl(packedData);
      buffer = (void *) malloc(sizeof(void *) * chunkSize);
      //set the file offset for writing since we are only going to be 
      //writing a chunk of data after each receive.
      offset = 0;

      fd = open(file, O_RDWR | O_CREAT, mode);
      if(fd == -1) {
         perror("Could not open file");
         return -1;
      }

      while(totalRecv < fileSize) {

         if((bytesRecv = recvAll(socket, buffer, chunkSize)) == -1) {
            close(fd);
            perror("Could not recv file.");
            return -1;
         }
         offset = totalRecv += bytesRecv;

         if((pwrite(fd, buffer, bytesRecv, offset)) == -1) {
            close(fd);
            perror("Could not write to file.");
            return -1;
         }
      }
      free(buffer);
      close(fd);
   }

   return 0;
}

int protocolMain(int socket) {

   char *cmd, *arg;
   int  cmdLen, argLen;
   uint32_t contentLength;
   int status;
   
   //get the length of the command. The client will make sure
   //the sender never sends an empty command
   status = recvAll(socket, &contentLength, sizeof(uint32_t));
   if(status == -1)
      return status;
   cmdLen = ntohl(contentLength);
   cmd = (char*) malloc(sizeof(char*) * cmdLen);
   
   //get the actual command
   status = recvAll(socket, cmd, cmdLen); 
   if(status == -1)
      return status;
   cmd[status]='\0'; 

   //return if the client will not be sending anymore data
   if(strcmp(cmd, "bye") == 0) {
      return -1;
   }

   if(strcmp(cmd, "catalog") == 0) {
      catalog(socket);
      free(cmd);
      return 0;
   }

   //get the argument
   status = recvAll(socket, &contentLength, sizeof(uint32_t));
   if(status == -1)
      return status;

   argLen = ntohl(contentLength);
   arg = (char*) malloc(sizeof(char*) * argLen);
   arg[status]='\0';

   if(strcmp(cmd, "ucopy") == 0) {
      
      status = ucopy(socket, arg);
   }
   else {
      status = dcopy(socket, arg);
   }
   
   free(cmd);
   free(arg);
   return status;


}
