#include<sys/socket.h>

//This function writes a list of the current directory to a socket
//
int catalog(int socket);


//This function sends a the file named by char *file to the client
//over the socket descriptor socket. If the file does not exist on 
//the server, the the server will send the NOT_FOUND error code and
//exits. There is no return value. If there is an error in the call
//to send that is not recoverable, then the function returns -1 and
//exits.
int dcopy(int socket, char *file);


//this function attempts to receive the file named by char *file 
//from the client. 
int ucopy(int socket, char *file);


//This function runs through all of the steps in the communication
//protocol between the client and the server. The protocol is as 
//follow:
//
// *NOTE: In the case of receiving commands or data, the server or 
//        client must first send the length of this data. All
//        content lengths will be sent as 4 byte uint32_t so the 
//        recipient does not need to know the length of the length.
//
//
// 1. The server waits for the command from the client.
// 2. set num_args based on the command
// 3. then a DONE code from the client
// 4. if cmd == BYE, close the client socket
// 5. if cmd == CATALOG, call catalog()
// 6. if cmd != UCOPY && cmd != DCOPY, send INVALID_CMD reply
// 7. if args1 == NULL && args2 == NULL, send USAGE reply
// 8. if cmd == UCOPY, call ucopy()
// 9. if cmd == DCOPY, call dcopy()

int protocolMain(int socket);



//Gets a list of the files in the current directory, writing them
//to the char **listing variable. The char listing variable will
//be allocated if it is not already, but will not be free'd. The caller
//must be sure to free listing when it is no longer needed.
//
void ls(char **listing);


//This function checks to see if the file named in the parameter exists
//on the server. Returns 0 if it does not exist, and 1 if it is found.
int find(char *fileName);



