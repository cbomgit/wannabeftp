#include "serverFunctions.h"
#include "debug.h"
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<arpa/inet.h>

#define BACKLOG 10
#define PORT "34900"


int buildConnection() {

   int sockfd;
   struct addrinfo hints, *servinfo, *p;
   int yes = 1, rv;
   void *addr;
   char ipstr[INET6_ADDRSTRLEN];

   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_flags = AI_PASSIVE; //use my IP

   #ifdef DEBUG
      printf("Not using ipv6.\n");
   #endif

   if((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
      return 1;
   }

   for(p = servinfo; p != NULL; p = p-> ai_next) {
      if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
         perror("server: socket");
         continue;
      }

      if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
         perror("setsockopt");
         exit(EXIT_FAILURE);
      }

      if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
         close(sockfd);
         perror("server: bind");
         continue;
      }

      break;
   }

   if(p == NULL) {
      fprintf(stderr, "server:failed to bind\n");
      return 2;
   }

   #ifdef DEBUG
      struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
      addr = &(ipv4->sin_addr);
      // convert the IP to a string and print it:
      inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
      printf("Listening on  %s:%s\n", ipstr, PORT);
   #endif

   freeaddrinfo(servinfo); 

   if(listen(sockfd, BACKLOG) == -1) {
      perror("listen");
      exit(EXIT_FAILURE);
   }
   
   return sockfd;

}



int sendAll(int socket, void *buff, int len) {

   int total, sent;
   uint32_t packed;

   total = sent = 0;

   while(total < len) {
     sent = send(socket, buff + total, len - total, 0);
     if(sent == -1) {
         return -1;
     }
     total += sent; 
   }

   return total;
}

int recvAll(int socket, void *buff, int len) {

   int total, recvd;
   total = recvd = 0;
   while(total< len) {
      recvd = recv(socket, buff + total, len - total, 0);
      if(recvd == -1) {
         return -1;
      }
      total += recvd;
   }
   return recvd;
}

void *get_in_addr(struct sockaddr *sa) {

   if(sa->sa_family == AF_INET) {
      return &(((struct sockaddr_in*)sa)->sin_addr);
   }

   return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
