#include"protocol.h"
#include"clientFunctions.h"
#include<string.h>
#include<stdio.h>
#include<stdint.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<dirent.h>
#include<stdint.h>
#include<sys/stat.h>


int catalog(int socket) {

   char *listing;
   char cmd[] = "catalog";
   int len, bytes;
   uint32_t packedData;


   //send the command and length of the command to the server
   packedData = htonl(strlen(cmd));
   if((bytes = sendAll(socket, &packedData, sizeof(uint32_t))) == -1)
   {
      perror("Could not send cmd length. sendAll()");
      return -1;
   }

   if((bytes = sendAll(socket, cmd, strlen(cmd))) == -1) {
      perror("Could not send cmd. sendAll()");
      return -1;
   }

   //now wait for the length of the returned string, and the string itself
   //
   if((bytes = recvAll(socket, &packedData, sizeof(uint32_t))) == -1) {
      perror("Could not receive content length. recvAll()");
      return -1;
   }

   len = ntohl(packedData);
   listing = (char *) malloc(sizeof(char*) * len);
   if(listing == NULL) {
      printf("Error allocating system resources. Aborting.\n");
      close(socket);
      exit(EXIT_FAILURE);
   }

   if((bytes = recvAll(socket, listing, len)) == -1) {
      perror("Could not receive directory contents.");
      free(listing);
      return -1;
   }

   listing[bytes] = '\0';

   printf("%s", listing);
   return 0;

}
      
int find(char *fileName) {

   DIR *d;
   struct dirent *dir;
   d = opendir(".");

   if(d) {
      while((dir = readdir(d)) != NULL) {
         if(dir->d_type == DT_REG && strcmp(dir->d_name, fileName) == 0) {
            return 1;
         }
      }
   }

   return 0;

}

int dcopy(int socket, char *src, char *dest) {

   int fileSize, fileDesc, chunkSize = 4096;
   uint32_t packedData;
   void *buffer;

   //set the right file permissions
   mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

   //file offset to read from when looping over multiple sends
   off_t offset;

   //used in send loop
   int bytesRecv, totalRecv; 

   if(find(dest)) {
      printf("File already exists on this computer.\n");
      return 0;
   }
   
   //get the fize of the file
   if((bytesRecv = recvAll(socket,&packedData,sizeof(uint32_t))) == -1) {
      perror("Could not receive file size. recvAll()");
      return -1;
   }

   fileSize = ntohl(packedData);
   if(fileSize == 0) {
      printf("This file already exists on the client.\n");
      return 0;
   }

   buffer = (void*) malloc(sizeof(void*) * chunkSize);
   offset = 0;
   fileDesc = open(dest, O_RDWR | O_CREAT, mode);
   if(fileDesc == -1) {
      perror("Could not open file for writing");
      return -1;
   }

   buffer = (void*) malloc(sizeof(void*) * chunkSize);
   if(buffer == NULL) {
      printf("Could not allocated system resources. Aborting.\n");
      close(socket);
      exit(EXIT_FAILURE);
   }

   while(totalRecv < fileSize) {

      if((bytesRecv = recvAll(socket, buffer, chunkSize)) == -1) {
         close(fileDesc);
         perror("Could not receive file");
         free(buffer);
         return -1;
      }
      offset = totalRecv += bytesRecv;

      if((pwrite(fileDesc, buffer, bytesRecv, offset)) == -1) {
         close(fileDesc);
         free(buffer);
         perror("pread()");
         return -1;
      }
   }

   free(buffer);
   close(fileDesc);

   return 0;

}
   
    
int ucopy(int socket, char *file) {

   int fd, fileSize, chunkSize = 4096;
   int bytesRead = 0, totalRead;
   off_t offset;
   uint32_t *packed;
   void *buffer;
   struct stat st;

   //get the size of the file
   if((recvAll(socket, packed, sizeof(uint32_t))) == -1) {
      perror("Error receiving confirmation from server");
      return -1; 
   }

   //only proceed if the file does not exist on the server
   if(( ntohl(*packed)) == 0) {

      //send the file size
      stat(file, &st);
      *packed = htonl(st.st_size);
      if((sendAll(socket, &packed, sizeof(uint32_t))) == -1) {
         perror("Error sending file size.");
         return -1;
      }
       
      buffer = (void*) malloc(sizeof(void*) * chunkSize);

      //open the file for reading
      fd = open(file, O_RDONLY);
      if(fd == -1) {
         fprintf(stderr, "Could not open file for reading.\n");
         exit(EXIT_FAILURE);
      }

      offset = 0;

      while(totalRead < fileSize) {
         bytesRead = pread(fd, buffer, chunkSize, offset);
         if(bytesRead == -1) {
            close(socket);
            free(buffer);
            close(fd);
            exit(EXIT_FAILURE);
         }

         totalRead += bytesRead;
         offset = totalRead;

         if((sendAll(socket, buffer, bytesRead)) == -1) {
            free(buffer);
            close(socket);
            close(fd);
            exit(EXIT_FAILURE);
         }
      }

      close(fd);
      free(buffer);

   }

   return 0;
}

//client prompts for input and ensures that the user enters
//a valid command with valid syntax. Client also makes sure source
//files don't already exist before initiating a download, and that 
//they do exist on the client when initiating an upload
int protocolMain(int socket) {

   const int CMD_MAX = 8;

   //the maximum length of input is the length of the longest command string
   //(catalog), plus a space, plus the maximum length of a file name, plus a
   //space, plus another max file name length, and a new line appended by fgets.
   const int MAX_LENGTH = CMD_MAX + 1 + NAME_MAX + 1 + NAME_MAX;
   char *cmd, *srcFile, *destFile, input[MAX_LENGTH];
   uint32_t contentLength;
   int status;

   printf("> ");
   fgets(input, MAX_LENGTH, stdin);
   while(strcmp(input, "\n") == 0) {
      printf("> ");
      fgets(input, MAX_LENGTH, stdin);
   }

   input[strlen(input) - 1] = '\0'; //trim the new line character

   //tokenize, delimiting by spaces
   //we should at least have a cmd to send
   cmd = strtok(input, " ");

   //try and get arguments, if any were given
   if((srcFile = strtok(NULL, " ")) != NULL) {
      destFile = strtok(NULL, " ");
   }

   if(destFile == NULL && srcFile != NULL) {
      destFile = (char*) malloc(sizeof(char*) * strlen(srcFile));
      strcpy(destFile, srcFile);
   }

   //determine the command
   

    
}

  
   


   
