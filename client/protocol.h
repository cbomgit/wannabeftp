//Function definitions for commands that can be processed by the
//client and sent to the server. All commands return -1 if call
//to send or recv returns one. This informs the caller that 
//resources should released the the program should be aborted.
//The error should propagate all the way up to the main function.
//
//
#include<sys/socket.h>

//Runs through all the steps in the communication protocol between
//the server and client. The protocol is as follows:
//
//1. Client waits for input from user. It is the clients responsibility
//   to ensure the user has entered a valid command with valid syntax and
//   the appropriate number of arguments. The client should reprompt as necessary.
//2. Client sends cmds and any arguments to the server. Client should always first
//   send the length of each string to the server so that it knows how much to receive.
//3. Bye will terminate the connection and abort the program.
//4. catalog() will retrieve the contents of the current directory on the server
//5. dir is a local only action and will list the contents of the clients current directory
//6. ucopy will upload a file from the client to the server. This command takes two arguments:
//    a. The name of the file on the client and
//    b. The name of the file that will be created on the server.
//
//    If the file does not exist on the client, then no data is sent and user is reprompted.
//    If the dest file already exists on the server, then no further data is sent and user is
//    informed. If only one argument is given it is assumed that src and dest file name are the
//    same. The client and server agree to send data in 4096 byte chunks.
//7. dcopy will download a file from the server to the client. This command takes two arguments:
//    a. The name of the file on the server and
//    b. The name of the file that will be created on the client.
//
//    If the file does not exists on the server, a size of 0 will be returned and the client will
//    not attempt to recv() any data. If the specified name already exists on the client, then no
//    data will be received from the server (this check should go first). If only one argument is 
//    given, then src and dest are assumed to be the same. Server and client aggree to only send data
//    in 4096 byte chunks.
int protocolMain(int socket);


//list the contents of the current directory
int dir(char *file);



//client will ask for a listing of the servers current directory
int catalog(int socket);


//checks if a file exists in the current directory
int find(char *file);


//Downloads file named by *src and writes it to file named by *dest.
//All error checking has already been completed.
int dcopy(int socket, char *src, char *dest);


//Uploads a file named by *src and sends it to the server to write it 
//to the file named by *dest. All error checking should have already 
//been completed prior to calling this function.
int ucopy(int socket, char *file);

