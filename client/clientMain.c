#include"clientFunctions.h"
#include"protocol.h"
#include"debug.h"
#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>
#include<arpa/inet.h>
#include<string.h>
#include<stdint.h>
#include<unistd.h>


int main(int argc, char *argv[]) {

   int sockfd, bytes_sent, contentLength;
   char msg[NAME_MAX];
   uint32_t packedLength;

   if(argc != 3) {
      fprintf(stderr, "Usage: wannabeFTP [server-ip] [port]\n");
      exit(EXIT_FAILURE);
   }

   printf("Trying to connect to %s on port %s.\n", argv[1], argv[2]);
   sockfd = connectToServer(argv[1], argv[2]);
   if(sockfd == -1) {
      perror("connectToServer()");
      exit(EXIT_FAILURE);
   }
    
   catalog(sockfd);
   printf("Closing connection to server.\n");
   close(sockfd);
   return 0;
}

   
