///Header file defining common network related functions
//performed by the server.
#include<sys/socket.h>

//Function to set up a connection on the specified IP address
//and port number. Creates a socket and calls connection, returning
//the socket descriptor. If any calls to getaddrinfo, socket, or connect
//return -1, this function will abort the program.
int connectToServer(char *serverIp, char *portNo);


//Function to send data in buff over a socket. Attempts to call send()
//as many times as necessary to ensure that all data is sent. A maximum
//chunk size is defined here and any errors are handled internally. 
//Some values of errno indicate that we just need to try again, while
//others indicated we need to close the connection and listen for 
//more client connections.
int sendAll(int socket, void *buff, int len);


//Same as above except for a receive operation, so buff is a pointer to 
//the pointer that references our allocated memory. In this case, len
//is the amount of data we are expected to receive. Calls to recvAll()
//should be preceded by a call to fill len.
int recvAll(int socket, void *buff, int len);


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa);
