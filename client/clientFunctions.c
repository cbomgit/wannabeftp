#include "clientFunctions.h"
#include "debug.h"
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<arpa/inet.h>

#define BACKLOG 10
#define PORT "34900"


int connectToServer(char *serverIp, char *portNo) {

   int sockfd;
   struct addrinfo hints, *servinfo, *p;
   int rv;
   void *addr;
   char ipstr[INET6_ADDRSTRLEN];

   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_STREAM;

   #ifdef DEBUG
      printf("Not using ipv6.\n");
   #endif

   if((rv = getaddrinfo(serverIp, portNo, &hints, &servinfo)) != 0) {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
      return 1;
   }

   for(p = servinfo; p != NULL; p = p-> ai_next) {
      if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
         perror("client: socket");
         continue;
      }


      if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
         close(sockfd);
         perror("client: connect");
         continue;
      }

      break;
   }

   if(p == NULL) {
      fprintf(stderr, "Client failed to connect.\n");
      return 2;
   }

   #ifdef DEBUG
      struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
      addr = &(ipv4->sin_addr);
      // convert the IP to a string and print it:
      inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
      printf("Got IP address %s for %s on port %s.\n", ipstr, serverIp, portNo);
      printf("Made connection with server.\n");
   #endif

   freeaddrinfo(servinfo); 
   
   return sockfd;

}



int sendAll(int socket, void *buff, int len) {

   int total, sent;

   total = sent = 0;

   while(total < len) {
     sent = send(socket, buff + total, len - total, 0);
     if(sent == -1) {
         break;
     }
     total += sent; 
   }

   return sent;
}

int recvAll(int socket, void *buff, int len) {

   int total, recvd;
   total = recvd = 0;
   while(total< len) {
      recvd = recv(socket, buff + total, len - total, 0);
      if(recvd == -1) {
         break;
      }
      total += recvd;
   }
   return recvd;
}

void *get_in_addr(struct sockaddr *sa) {

   if(sa->sa_family == AF_INET) {
      return &(((struct sockaddr_in*)sa)->sin_addr);
   }

   return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
